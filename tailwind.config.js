/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./*.html"],
  theme: {
    extend: {
      colors: {
        'darkred': '#8E1515',
        'graycustom': '#656262',        
        'gradient' : {
          50: 'rgba(236, 236, 236, 0.4)',
          500: 'rgba(236, 236, 236, 1)',
        }
      },
      fontFamily: {
        'body': ['Rajdhani', 'sans-serif'],
        'display': ['Nova Square', 'cursive'],
      },
    },
  },
  plugins: [],
}

